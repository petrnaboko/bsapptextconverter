﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextConverter
{
    public partial class Form1 : Form
    {
        private string fileContent { get; set; }
        private string finalContent { get; set; } = string.Empty;
        private string resultFileName { get; set; }
        public Form1()
        {
            InitializeComponent();

            // test commit
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    richTextBox1.Text = openFileDialog.FileName;
                    //var fileStream = openFileDialog1.OpenFile();
                    using(StreamReader reader = new StreamReader(openFileDialog.FileName))
                    {
                        fileContent = reader.ReadToEnd();
                        richTextBox2.Text = fileContent;
                        resultFileName = "remastered" + openFileDialog.SafeFileName;
                    }
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            var headerPart = false;
            var commonPart = false;
            int pos1 = 0;
            int pos2 = 0;
            int posCommon1 = 0;
            int posCommon2 = 0;
            var hLabelPart1 = "<Label Text=\"";
            var hLabelPart2 = "\" HorizontalOptions = \"Center\" FontAttributes = \"Bold\" FontSize=\"{ Binding Source = { x:Static local:AppSettings.FontSize}}\"/>";
            var labelParameter = 1;
            var cmnLabel1 = $"<Label x:Name=\"l{labelParameter}\"" + " FontSize=\"{ Binding Source = { x:Static local:AppSettings.FontSize}}\" >";
            var cmnLabel2 = "</Label>";
            string path = @"C:\Results\" + resultFileName;
            for (int i = 0; i < fileContent.Length; i++)
            {
                if (fileContent[i] == '|')
                {
                    if (!headerPart)
                    {
                        headerPart = true;
                        pos1 = i + 1;
                    }
                    else
                    {
                        pos2 = i - 1;
                        var sentence = fileContent.Substring(pos1, pos2 - pos1 + 1);
                        sb.Append(hLabelPart1 + sentence + hLabelPart2 + '\n' + '\n');
                        headerPart = false;
                    }
                }

                if (fileContent[i] == '@')
                {
                    if (!commonPart)
                    {
                        commonPart = true;
                        posCommon1 = i + 1;
                    }
                    else
                    {
                        posCommon2 = i - 1;
                        var sentence = fileContent.Substring(posCommon1, posCommon2 - posCommon1 + 1);
                        sb.Append(cmnLabel1 + sentence + cmnLabel2 + '\n' + '\n');
                        commonPart = false;
                        labelParameter++;
                        cmnLabel1 = $"<Label x:Name=\"l{labelParameter}\"" + " FontSize=\"{ Binding Source = { x:Static local:AppSettings.FontSize}}\" >";
                    }
                }
            }
            richTextBox3.Text = sb.ToString();

            File.WriteAllText(path, sb.ToString());
        }
    }
}
